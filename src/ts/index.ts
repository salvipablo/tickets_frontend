type interfaceLogin = import('./_types').interfaceLogin; 

const cntLoginForm = document.getElementById('cntLoginForm') as HTMLDivElement;
const cntButtons = document.getElementById('cntButtons') as HTMLDivElement;

const btnLogin = document.getElementById('btnLogin') as HTMLButtonElement;

const username = document.getElementById('username') as HTMLInputElement;
const password = document.getElementById('password') as HTMLInputElement;

const btn_cerrarS = document.getElementById('btn_cerrarS') as HTMLButtonElement;

const token = document.cookie.split('=')[1];

btn_cerrarS.addEventListener('click', () => {
  const fechaExpiracion = new Date('Thu, 01 Jan 1970 00:00:00 UTC');
  document.cookie = `tokenBlog=; expires=${fechaExpiracion.toUTCString()}; path='/tickets_frontend/src'`;
  window.location.reload();
});

if (!token) {
  cntLoginForm.classList.remove('hidden');
} else {
  cntButtons.classList.remove('hidden');
}

btnLogin.addEventListener('click', (e) => {
  e.preventDefault();

  let dataLogin: interfaceLogin = {
    username: username.value,
    password: password.value,
  };

  login(dataLogin);
});

const login = async (dataForLogin: interfaceLogin) => {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(dataForLogin),
  };

  try {
    const response = await fetch('http://localhost:5559/users/login/', requestOptions);

    if (!response.ok) {
      throw new Error('Los datos ingresados son incorrectos.');
    }

    const dataResponse = await response.json();

    console.log(dataResponse);
    
    document.cookie = "tokenBlog =" + dataResponse.data + ";"

    window.location.href = "index.html";
  } catch (error: any) {
    alert(error.message);
  }
};