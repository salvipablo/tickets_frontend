type interfaceProfileUser = import('./_types').interfaceProfileUser; 

const app = document.getElementById('app') as HTMLDivElement;

const inpFirstname = document.getElementById('firstname') as HTMLInputElement;
const inpLastname = document.getElementById('lastname') as HTMLInputElement;
const inpEmail = document.getElementById('email') as HTMLInputElement;
const inpUsername = document.getElementById('username') as HTMLInputElement;

const btnProfileForm = document.getElementById('btnProfileForm') as HTMLButtonElement;

const profileRequest = async () => {
  const token = document.cookie.split('=')[1];

  if (!token) {
    alert('No esta autorizado para la solicitud de perfil, debe ingresar en el sistema');
    window.location.href = "index.html";
    return;
  }

  const url = `http://localhost:5559/users/profile?token=${token}`;
  const options = { method: 'GET' };
  const request = await fetch(url, options);

  const response = await request.json();

  return response;
};

btnProfileForm.addEventListener('click', async (e) => {
  e.preventDefault();

  const token = document.cookie.split('=')[1];

  if (!token) {
    alert('No esta autorizado para la solicitud, debe ingresar en el sistema');
    window.location.href = "index.html";
    return;
  }

  const url = `http://localhost:5559/users/profile?token=${token}`;

  let userData = {
    firstname: inpFirstname.value,
    lastname: inpLastname.value,
    email: inpEmail.value,
    username: inpUsername.value
  };

  const options = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(userData)
  };
  
  const request = await fetch(url, options);
  const response = await request.json();
  
  if (response.message === 'User updated successfully') {
    alert(response.message);
    window.location.href = "index.html";
  }
});

const init = async () => {
  try {
    const profileUser: interfaceProfileUser = await profileRequest();

    inpFirstname.value = profileUser.data.firstname;
    inpLastname.value = profileUser.data.lastname;
    inpEmail.value = profileUser.data.email;
    inpUsername.value = profileUser.data.username;
  } catch (error: any) {
    alert(`No se pudo realizar la peticion de informacion de perfil: ${error.message}`);
  }
};

init();
