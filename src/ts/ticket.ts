function initTicket() {
  const valores = window.location.search;
  const urlParams = new URLSearchParams(valores);

  const urlPhoto  = urlParams.get('urlPhoto') || '';
  const idTicket = urlParams.get('idTicket');
  const nameTicket = urlParams.get('nameTicket') || '';
  const description = urlParams.get('description');

  const titleMainT = document.getElementById('titleMainT');
  const descriptionT = document.getElementById('descriptionT');
  const idTicketText = document.getElementById('idTicketText');
  const imgT = document.getElementById('imgT') as HTMLImageElement;

  titleMainT!.textContent = nameTicket;
  descriptionT!.textContent = `Description ticket: ${description}`
  idTicketText!.textContent = `ID: ${idTicket}`;
  imgT!.src = urlPhoto;
};

initTicket();