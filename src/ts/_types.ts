export interface interfaceProfileUser {
  data: {
    firstname: string,
    lastname: string,
    email: string,
    username: string,
  }
};

export interface interfaceLogin {
  username: string,
  password: string,
};