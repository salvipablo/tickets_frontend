const titleList = document.getElementById('titleList') as HTMLTitleElement;
const tableDataTickets = document.getElementById('tableDataTickets') as HTMLTableElement;
const formTicket = document.getElementById('projectFrm') as HTMLFormElement;

const valores = window.location.search;
const urlParams = new URLSearchParams(valores);
const idProject: string = urlParams.get('idProject') || '';
const nameProject = urlParams.get('nameProject');

interface Ticket {
  nameTicket: string,
  description: string,
  idTicket: string,
  idProject: string,
  urlPhoto: string
};

formTicket.addEventListener('submit', (e) => {
  e.preventDefault();
  saveNewTicket();
});

const saveNewTicket = async () => {
  const ticketName = document.getElementById('ticketName') as HTMLInputElement;
  const ticketDescription = document.getElementById('ticketDescription') as HTMLTextAreaElement;
  const idTicket = document.getElementById('idTicket') as HTMLInputElement;
  const photo = document.getElementById('photo') as HTMLInputElement ;

  const formData = new FormData();
  formData.append('ticketName', ticketName.value);
  formData.append('idTicket', idTicket.value);
  formData.append('ticketDescription', ticketDescription.value);
  formData.append('photo', photo.files![0]);

  const token = document.cookie.split('=')[1];

  if (!token) {
    alert('No esta autorizado para la solicitud, debe ingresar en el sistema');
    window.location.href = "index.html";
    return;
  }
  const url = `http://localhost:5559/tickets?idProject=${idProject}&token=${token}`;

  const options = {
    method: 'POST',
    body: formData
  };

  try {
    await fetch(url, options);
  } catch (error: any) {
  }
  alert('Ticket saved successfully');
  window.location.reload();
};

async function getDataForTable(): Promise<Ticket[]> {
  const token = document.cookie.split('=')[1];

  if (!token) {
    alert('No esta autorizado para la solicitud, debe ingresar en el sistema');
    window.location.href = "index.html";
  }

  const url = `http://localhost:5559/tickets?idProject=${idProject}&token=${token}`;

  const options = { method: 'GET' };
  
  const request = await fetch(url, options);

  const response = await request.json();

  let ticketsForTable: Ticket[] = response.data;

  return ticketsForTable;
}

async function updateTable() {
  let tickets: Ticket[] = await getDataForTable();

  let htmlAgregate = `
    <tr class="tableData-row">
      <th class="tableData-th1 borderRow">ID Ticket</th>
      <th class="tableData-th2 borderRow">Name</th>
      <th class="tableData-th3 borderRow">Description</th>
      <th class="tableData-th4 borderRow">View</th>
    </tr>
  `;

  tickets.forEach(element => {
    htmlAgregate += `
      <tr class="tableData-row">
        <td class="tabledata-cell borderRow center">${element.idTicket}</td>
        <td class="tabledata-cell borderRow">${element.nameTicket}</td>
        <td class="tableData-th3 borderRow">${element.description}</td>
        <td class="tabledata-cell borderRow center">
          <a href="ticket.html?urlPhoto=${element.urlPhoto}&idTicket=${element.idTicket}&nameTicket=${element.nameTicket}&description=${element.description}">...</a>
        </td>
      </tr>
    `;
  });
  
  tableDataTickets.innerHTML = htmlAgregate;
};

function initProject() {
  titleList.innerHTML = `Listado de tickets del proyecto: ${nameProject}`
  updateTable();
}

initProject();