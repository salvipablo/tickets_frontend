const btnForm = document.getElementById('btnForm') as HTMLButtonElement;
const projectsName = document.getElementById('projectsName') as HTMLInputElement;
const descriptionTextA = document.getElementById('description') as HTMLTextAreaElement;

const tableData = document.getElementById('tableData') as HTMLTableElement;

interface Project {
  _id?: string,
  nameProject: string,
  description: string
};

btnForm.addEventListener('click', (e) => {
  e.preventDefault();

  const newProject: Project = {
    nameProject: projectsName?.value,
    description: descriptionTextA?.value
  }

  addNewProject(newProject);
});

async function addNewProject(dataForProject: Project) {
  const token = document.cookie.split('=')[1];

  if (!token) {
    alert('No esta autorizado para la solicitud, debe ingresar en el sistema');
    window.location.href = "index.html";
    return;
  }

  const url = `http://localhost:5559/projects?token=${token}`;

  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(dataForProject)
  };
  
  const request = await fetch(url, options);
  const response = await request.json();
  
  alert(response.message);

  projectsName.value = '';
  descriptionTextA.value = '';
  
  window.location.reload();
}

async function initProjects() {
  const token = document.cookie.split('=')[1];

  if (!token) {
    alert('No esta autorizado para la solicitud, debe ingresar en el sistema');
    window.location.href = "index.html";
    return;
  }

  const url = `http://localhost:5559/projects?token=${token}`;

  const options = {
    method: 'GET',
  };

  const request = await fetch(url, options);
  const response = await request.json();

  if (response.message === 'Sending of all projects of the database') {
    createTable(response.data);
  }
}

function createTable(data: Array<Project>) {
  let htmlAgregate = `
    <tr class="tableData-row">
      <th class="tableData-th1 borderRow">Name Project</th>
      <th class="tableData-th2 borderRow">Description</th>
      <th class="tableData-th3 borderRow">Ver Proyecto</th>
    </tr>
  `;

  data.forEach(element => {
    htmlAgregate += `
      <tr class="tableData-row">
      <td class="tabledata-cell borderRow center">${element.nameProject}</td>
      <td class="tabledata-cell borderRow">${element.description}</td>
      <td class="tabledata-cell borderRow center">
        <a href="project.html?idProject=${element._id}&nameProject=${element.nameProject}">...</a>
      </td>
      </tr>
    `;
  });

  tableData.innerHTML = htmlAgregate;
}

initProjects();